const x = document.getElementById('bucket_cap_1');
const y = document.getElementById('bucket_cap_2');
const d = document.getElementById('desired_amount');
const my_form = document.querySelector('#my-form');
const msg = document.querySelector('.msg');
const userList = document.querySelector('#outputs');


function isNatural(data) {
    return (data % 1 === 0) && (data >= 1);
}

my_form.addEventListener('submit', _submit);

function _submit(e) {
    e.preventDefault();

    if (!isNatural(x.value) || !isNatural(y.value) || !isNatural(d.value)){
        msg.classList.add('error');
        msg.innerHTML = "Error: Invalid input -> all inputs must be natural numbers."
        setTimeout(() => msg.remove(), 5000)
    }

    else{
        const li = document.createElement('li');
        li.appendChild(document.createTextNode(`${buckets()}`));
        userList.appendChild(li);
        x.value = '';
        y.value = '';
        d.value = '';
    }

}


function buckets(){
    if (((x.value > d.value) && (y.value > d.value)) || ((x.value < d.value) && (y.value < d.value))){
        return "Invalid inputs. One bucket must be less than or equal to the desired bucket, " +
            "and one bucket must be greater than or equal to the desired bucket."
    }
    else if(x.value === d.value){
        return "Simply fill up the first bucket."
    }
    else if(y.value === d.value){
        return "Simply fill up the second bucket."
    }
    else if((d.value % Math.min(x.value, y.value)) === 0){
        return `Fill up the smaller bucket and empty it into the larger bucket ` +
            `${d.value / Math.min(x.value, y.value)} times.`
    }
    else {
        let n = Math.min(x.value, y.value);
        const min_val = Math.min(x.value, y.value);
        const max_val = Math.max(x.value, y.value);
        let counter = 1;
        while (n < max_val) {
            n += min_val;
            counter += 1;
        }
        if ((d.value - (n % max_val)) % min_val === 0){
            return `For bucket 1 having a ${x.value} gallon capacity, bucket 2 having a ${y.value} gallon capacity, 
            and a desired amount of ${d.value} gallons: •Fill up the small bucket and dump it into the larger bucket 
            ${counter} times until the larger bucket is completely full, while keeping the remaining ${n%max_val} 
            gallon(s) of water inside the smaller bucket. •Then, dump out all the water in the larger bucket. •Once 
            the large bucket is empty, we can dump the ${n%max_val} gallon(s) from the smaller bucket into the larger 
            bucket. •We then fill up the smaller bucket and empty it into the larger bucket 
            ${((d.value - (n % max_val)) / min_val)} time(s) to achieve our desired amount of ${d.value} gallons.`
        }
        else{
            return "Desired bucket amount is not possible."
        }
    }
}
